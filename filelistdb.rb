#!/usr/bin/ruby1.9


require 'fileutils'
require 'rubygems'
require 'sqlite3'
require 'nokogiri'
require 'open-uri'
require 'optparse'
require 'ostruct'

MEMDBName = ":memory:"

class String
    def sql_quote
        self.to_s.gsub(/\\/, '\&\&').gsub(/'/, "''")   #')
    end
end


#----------------------------------------------------------------------------
#       control repositories
#----------------------------------------------------------------------------
WorkDir = "#{Dir.home}/tmp/rpmfiles"
DbDir = "#{Dir.home}/lib/rpmfiles"
FileUtils.mkdir_p WorkDir 
FileUtils.mkdir_p DbDir 


class Repo
    attr_reader :basePath, :repoName
    def initialize(basePath, repoName)
        @basePath = basePath
        @repoName = repoName
    end

protected
    def getfileListUrl
        url = nil
        doRetryFlag = true
        begin
            open(fileListXmlUrl) do |f|
                doc = Nokogiri::HTML(f, 'UTF-8')
                url = doc.at_xpath("//data[@type='filelists'] //location")["href"]
            end
        rescue OpenURI::HTTPError => e
            newUrl = @basePath + "suse/repodata/repomd.xml"
            if doRetryFlag then
                doRetryFlag = false
                @basePath += "suse/"
                @fileListXmlUrl = newUrl
                retry
            end
            raise e
        end

        @basePath + url
    end
    
public
#     def downloadFileListXml
#         fileListUrl = @currentRepo.fileListUrl
#         puts "xml file url:" + fileListUrl
#         %x{ curl -L #{fileListUrl} | gunzip - >#{@currentRepo.xmlFile} }
#     end

    def fileListXmlUrl
        @fileListXmlUrl ||= @basePath + "repodata/repomd.xml"
    end

    def fileListUrl
        @fileListUrl ||= getfileListUrl
    end

    def xmlFile
        @xmlFile ||= File.join(WorkDir, "filelist-#{@repoName}.xml")
    end

    def tmpDir
        @tmpDir ||= File.join(WorkDir, "tmp-#{@repoName}")
    end

    
#     def dbName
#         @dbName ||= File.join(DbDir, "rpmfiles-#{@repoName}.db")
#     end
    
    def self.masterDbName
        @@dbName ||= File.join(DbDir, "rpmfiles.db")
    end
end


RepoDataDir = "/etc/zypp/repos.d/"

def readRepo(file)
    open(file) do |f|
        repoName = /\[(.*)\]/.match( f.readline )[1]
        f.readlines.each do |l|
            key, val = l.strip.split(/=/, 2)
            case key
            when "baseurl"
                if val =~ /^(http|ftp):/ then
                    @repos[repoName] = Repo.new( val, repoName )
                end
            end
        end
    end
end
            
def readRepos
    @repos = {}
    Dir[RepoDataDir + "*.repo"].each do |f|
        readRepo(f)
    end
end

def listRepos
    puts "all repositories."
    unless @repos then
        readRepos
    end
#     maxsz = @repos.keys.max { |k| k.length }.to_s
    @repos.each_pair do |k,v| puts k, %Q!     "#{v.basePath}"! end
end

def matchedRepos(name)
    re = Regexp.new( Regexp.escape(name), true )
    @repos.keys.select do |repo|
        re.match( repo )
    end
end


                      
#----------------------------------------------------------------------------
#       download findlists.xml file
#----------------------------------------------------------------------------

def downloadFileListXml
    fileListUrl = @currentRepo.fileListUrl
    puts "xml file url:" + fileListUrl
    %x{ curl -L #{fileListUrl} | gunzip - >#{@currentRepo.xmlFile} }
end


# TmpDir = "tmp"
XmlHeader = <<-XML
<?xml version="1.0" encoding="UTF-8"?>
XML
FirstXmlClose = "</filelists>\n"

@totalPackages = nil

def splitFileListXml
    # clear working directory
    FileUtils.rm_rf(@currentRepo.tmpDir)
    FileUtils.mkdir(@currentRepo.tmpDir)


    def openWriteFile(no)
        puts "write part '#{no}' xml file."
        noStr = sprintf("%03d", no)
        fileName = File.join(@currentRepo.tmpDir, "filelist-#{noStr}.xml")
        open(fileName, "w")
    end

    lineLimit = 10000
    lineSplitCount = lineLimit 
    packCount = 0
    #
    open(@currentRepo.xmlFile) do |f|
        lineCount = 0
        firstFlag = true
        

        no = 0
        w = openWriteFile(no)
        while (!f.eof?) do
            line = f.readline
            lineCount += 1
            if line =~ /^<package/ then
                packCount += 1
                char = /name="(\w)/.match(line)[1].downcase
                if lineCount < lineSplitCount  then
                    w.write(line)
                else
#                     puts " line:#{line}"
                    if firstFlag then
                        firstFlag = false
                        w.write(FirstXmlClose)
                    end
                    w.close
                    puts "#{no} lines:#{lineCount}"


                    no += 1
                    w = openWriteFile(no)
                    w.write(XmlHeader)
                    w.write(line)

                    lineCount = 0
                end
            else
                if line !~ /<\/filelists>/ then
                    w.write(line)
                end
            end
        end
        w.close
    end
    puts "Total packages : #{packCount}"
    @totalPackages = packCount
end


#----------------------------------------------------------------------------
#       databas control
#----------------------------------------------------------------------------
                      
def updateRepo(repo)
    @currentRepo = repo
    if !File.exist?(@currentRepo.xmlFile) || @options.refreshXml then
        begin 
            downloadFileListXml
        rescue OpenURI::HTTPError => e
            puts "Error : repository (#{@currentRepo.repoName})read error!"
            return
        end
            
    end
    splitFileListXml

    unless defined? @db
        openMasterDB
    end

    cleanupOldRepoFiles
    insertAllFileList

    FileUtils.rm_rf(@currentRepo.tmpDir)
end

def getTotalPackages
    @totalPackages ||= %x{grep "<package" #{@currentRepo.xmlFile} | wc -l}.to_i
end


def cleanupOldRepoFiles
    sqlq = <<-SQL
        select id from reponames where name is '#{@currentRepo.repoName.sql_quote}'
    SQL
    @repoId = @db.get_first_value(sqlq)
    puts "@repoId : #{@repoId.inspect}"
    if !@repoId.nil? and @repoId > 0 then
        deleteCurrentRepo
    else
        insertRepoName
    end
end

def insertRepoName
    sqlq = <<-SQL
        select id from reponames where name is '#{@currentRepo.repoName.sql_quote}'
    SQL
    @repoId = @db.get_first_value(sqlq)
    return if @repoId and @repoId > 0

    sqlins = <<-SQL
        insert into reponames (name) values ('#{@currentRepo.repoName.sql_quote}')
    SQL
    @db.execute( sqlins )
    @repoId = @db.last_insert_row_id
end

def openMasterDB
    puts "openMasterDB #{Repo.masterDbName}"
    if File.exist?(Repo.masterDbName) then
        puts "loadToMemDB"
        @db = SQLite3::Database.new(MEMDBName)
        loadToMemDB
    else
        puts "createDB"
        createDB
    end
end

def createDB
    @db = SQLite3::Database.new(MEMDBName)
    sqlrepo = <<-SQL
        create table reponames( name text, id integer unique not null primary key asc autoincrement)
    SQL
    @db.execute( sqlrepo )

    sqlrpm = <<-SQL
        create table rpmnames( name text, full_name text, arch text, epoch text, ver text, rel text,
        repo_id, id integer unique not null primary key asc autoincrement,
        foreign key(repo_id) references reponames(id) )
    SQL
    @db.execute( sqlrpm )

    sqlfile = <<-SQL
        create table rpmfiles ( id, file_name text, base_name text,
            foreign key(id) references rpmnames(id) )
    SQL
    @db.execute( sqlfile )

end


def deleteCurrentRepo
    puts "deleteCurrentRepo"
    $stdout.flush
    # delete files
    offset = 0
    limit = 10000
    begin
        sql = <<-SQL
            select id from rpmnames where repo_id is #{@repoId} limit #{limit} offset #{offset}
        SQL
#         puts sql
        count = 0
        @db.execute( sql ) do |row|
            count += 1
            rpmId = row[0]
            sqldel = <<-SQL
                delete from rpmfiles where id = #{rpmId}
            SQL
            puts sqldel
            @db.execute( sqldel ) 
        end
        offset += count
    end while count == limit

    # delete rpm
    sqldelrpm = <<-SQL
        delete from rpmnames where repo_id is #{@repoId}
    SQL
    puts sqldelrpm
    @db.execute( sqldelrpm )
end

def getRpmIdByFullName(name)
    sqlq = <<-SQL
        select id from rpmnames where full_name is '#{name.sql_quote}' and repo_id is #{@repoId}
    SQL

    id_ret = @db.get_first_row( sqlq )
    if id_ret && id_ret[0] then
        id_ret[0].to_i
    else
        nil
    end
end


def insertFileList(doc)
    packagesDoc = doc.xpath("//package")
    packagesDoc.each do |pkg|
        name = pkg[:name]
        arch = pkg[:arch]
        verDoc = pkg.at_xpath("version")
        epoch = verDoc[:epoch]
        ver = verDoc[:ver]
        rel = verDoc[:rel]
        files = pkg.xpath("file[not (@type)]")
        full_name = name + "-" + ver + "-" + rel + "." + arch
        
        sql = <<-SQL
            insert into rpmnames (name, full_name, arch, epoch, ver, rel, repo_id)
                values ('#{name.sql_quote}', '#{full_name.sql_quote}', '#{arch}', '#{epoch}', '#{ver}', '#{rel}', #{@repoId} )
        SQL
#         puts sql
        puts "package #{@packCount} / #{@totalPackages}"
        @db.execute( sql )
        id = @db.last_insert_row_id
        puts "#{full_name} ID:#{id}"
        
        idnm = getRpmIdByFullName(full_name)
        if id != idnm then
            @error_id_conflict_count += 1
#             puts sql
            puts "inserted id #{id} != id by name #{idnm} # <<<<< different id !!!!!'"
#             sqlq = <<-SQL
#                 select * from rpmnames where full_name is '#{full_name.sql_quote}' and repo_id is #{@repoId}
#             SQL
#             rows = @db.execute( sqlq )
#             puts rows.inspect
#             raise "name id mismatch."
        end
        puts "write #{files.size} files"
        $stdout.flush if files.size > 1000
        files.each do |fileXml|
            file = fileXml.text
            baseName = File.basename(file)
            sqli = <<-SQL
                insert into rpmfiles (id, file_name, base_name)
                      values (#{id}, '#{file.sql_quote}', '#{baseName.sql_quote}')
            SQL
#             puts sqli
            @db.execute( sqli )
        end
        @packCount += 1
    end
end

def insertAllFileList
    @error_id_conflict_count = 0
    getTotalPackages
    @packCount = 0
    allFiles = Dir[@currentRepo.tmpDir + "/*"].sort
#     puts "insert files ; " + allFiles.inspect
    allFiles.each do |file|
        puts "read #{file}"
        xml =  open(file) do |f| f.read end
        doc = Nokogiri::HTML(xml)
        insertFileList(doc)
    end
end

def loadToMemDB
    puts "load from database on disk."
    $stdout.flush
    sqlread = <<-SQL
        attach database '#{Repo.masterDbName}' as fileDB;
        create table reponames( name text, id integer unique not null primary key asc autoincrement);

        create table rpmnames( name text, full_name text, arch text, epoch text, ver text, rel text,
        repo_id, id integer unique not null primary key asc autoincrement,
        foreign key(repo_id) references reponames(id) );

        create table rpmfiles ( id integer, file_name text, base_name text,
            foreign key(id) references rpmnames(id) );

        insert into reponames select * from fileDB.reponames;
        insert into rpmnames select * from  fileDB.rpmnames;
        insert into rpmfiles select id, file_name, base_name from  fileDB.rpmfiles;
        
        detach database fileDB;
    SQL
    @db.execute_batch(sqlread)
end

def saveMemDB
    return unless @db
    puts "save to database on disk."
    $stdout.flush
    File.delete( Repo.masterDbName )
    sqldump = <<-SQL
        attach database '#{Repo.masterDbName}' as fileDB;
        
        create table fileDb.reponames( name text, id integer unique not null primary key asc autoincrement);
        
        create table fileDb.rpmnames( name text, full_name text, arch text, epoch text, ver text, rel text,
        repo_id, id integer unique not null primary key asc autoincrement,
        foreign key(repo_id) references reponames(id) );
        
        create virtual table fileDb.rpmfiles using fts4( id, file_name text, base_name text,
            foreign key(id) references rpmnames(id) );
            
        insert into fileDB.reponames select * from reponames;
        insert into fileDB.rpmnames select * from rpmnames;
        insert into fileDB.rpmfiles( id, file_name, base_name ) select id, file_name, base_name from rpmfiles;
    SQL
    
    @db.execute_batch(sqldump)
end



#----------------------------------------------------------------------------
#       program start
#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
#       options
#----------------------------------------------------------------------------

# set default option parameters
@options = OpenStruct.new
@options.refreshXml = false
@options.command = nil
@options.verbose = false

# option parsing
pname = File.basename($PROGRAM_NAME)
opts = OptionParser.new do |o|
        o.banner = <<-EOF
find file from rpm.
Usage: #{pname} [options] repository_name
options :
        EOF
        o.on("-h", "--help", "help.") do |f|
            @options.command = :help
        end
        o.on("-v", "--verbose", "verbose mode.") do |f|
            @options.verbose = true
        end
        o.on("-l", "--list-repos", "list repositories.") do |f|
            @options.command = :listRepos
        end
        o.on("-c", "--cleanup", "clean up working directory.") do |f|
            @options.command = :cleanup
        end
        o.on("-x", "--refresh-xml", "refresh filelist.xml.") do |f|
            @options.refreshXml = true
        end
        o.on("-a", "--all", "update all repositories.") do |f|
            @options.command = :allRepos
        end
    end

begin
    opts.parse!(ARGV)
rescue OptionParser::InvalidOption
    puts opts
    exit 1
end

case @options.command
when :help
    puts opts
    exit 0
when :listRepos
    listRepos
    exit 0
when :cleanup
    FileUtils.rm_rf(WorkDir)
    exit 0
end


case ARGV.size
when 0
    name = "updates"
when 1
    name = ARGV.shift
else
    puts opts
    exit 1
end

readRepos

if @options.command == :allRepos then
    matches = @repos.keys
else
    matches = matchedRepos(name)
    case matches.size
    when 1

    when 0
        puts "no mathced repos"
        listRepos
        exit 1
    else
        repo = matches.find do |r| r == name end
        if repo then
            matches = [repo]
        else
            puts "more than one match for selecting repo."
            puts "matched repos."
            matches.each do |r| puts r end
            exit 1
        end
    end
end


#----------------------------------------------------------------------------
#       main
#----------------------------------------------------------------------------

matches.each do |name|
    puts "update repository : #{name}"
    updateRepo( @repos[name] )
end
saveMemDB

